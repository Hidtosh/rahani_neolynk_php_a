<?php
/** 
  * __author__ = "M'hand R" : 
**/
$randTab = array();
//fill random array
for ($i=0; $i < 20; $i++) {
	array_push($randTab, rand(0,100));
}
// function test user input in random array
function calculeSum($tabInt, $userInput){
	$result = "false";
	for ($i=0; $i < count($tabInt); $i++) {
	    for ($j = $i + 1; $j < count($tabInt); $j++) {
		    $sum = $tabInt[$i] + $tabInt[$j];
	    	if ($sum == $userInput)
	    		$result = "true";
		}
	}
	return $result;
}
$continu = "y"; 
// call to calculeSum to test user input
while (true){
	switch ($continu) {
    case "y": // rep yes continu
        echo "Write number please : ", PHP_EOL;
    	$userInput = strtolower(trim(fgets(fopen ("php://stdin","r"))));
		if (is_numeric($userInput))
		 	echo calculeSum($randTab, $userInput), PHP_EOL;
		else 
			echo "Try with correct number please", PHP_EOL;
        break;
    case "n": // no exit 
    	echo "Goodbye!", PHP_EOL;
        exit();
	}
	echo "Continue [Y/N]", PHP_EOL;
	$continu = strtolower(trim(fgets(fopen ("php://stdin","r"))));
}