# README #

### Quick test php ###
* Version 1.0

### Environnement de dev : ###
* Ubuntu 16.4 LTS
* Sublime Text3
* php7.0-cli (Terminal) 

### Test NeoLynk PHP A
Présentation : 

* 1 - 3.5 
* 2 - 1 ans
* 3 - Oui
* 4 - Élevé
* 5 - Comprendre le problème avant de commencer à coder && Bien documenter le code

Programmation orientée objet : 

* 1 - Le code est plus lisible pour moi
* 2 - 	Le pattern factory.
	    Le pattern observer.
	    Le pattern strategy.
	    Le pattern singleton.
	    Le pattern injection.
		
* 3 - Le pattern factory.
* 4 - :
* a - Oui le code est acceptable
* b - /
* c - Le pattern factory
* d - /

Micro projet : 
* - https://bitbucket.org/Hidtosh/rahani_neolynk_php_a/commits/f902a76b5e599554cc332a96d367081a01a0350d